# bkraft-securingc7

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with bkraft-securingc7](#setup)
    * [What bkraft-securingc7 affects](#what-bkraft-securingc7-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with bkraft-securingc7](#beginning-with-bkraft-securingc7)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Limitations - OS compatibility, etc.](#limitations)

## Overview

This module is pretty poorly written, I know - but the purpose of this module is to secure
the default CentOS 7 minimal installation.

## Module Description

This module:
* Installs and configures fail2ban
* Removes unnecessary kernel modules
* Narrows down rights on files and directories
* Denies AT/Crontab access to all users but root
* Enforces password default length
* Disconnects inactive users
* Secures up sshd
* Disables useless services
* Removes users
* Enforces the default UMASK

## Setup

### What bkraft-securingc7 affects

**Modified files/directories contents/rights**

* /etc/login.defs
* /etc/profile.d/umask-enforcement.sh
* /etc/cron.allow
* /etc/at.allow
* /etc/cron.deny
* /etc/at.deny
* /etc/issue
* /root
* /var/log/audit
* /etc/skel
* /etc/rsyslog.conf
* /sbin/iptables
* /etc/sysctl.d/securingc7.conf
* /etc/modprobe.d/blacklist.conf
* /etc/ssh/sshd_config

**Removed users**

* shutdown
* halt
* games
* operator
* ftp
* gopher
* lp

**Disabled services**

* iprinit
* iprupdate
* iprdump
* avahi-daemon
* NetworkManager
* firewalld

**Newly installed packages**

* iptables-services
* epel-release
* fail2ban

### Setup Requirements

Obviously, git and puppet basically

### Beginning with bkraft-securingc7

## Usage

Running all of it:
```
#!puppet

include bkraft-securingc7
```

All the other parts:
```
#!puppet

include bkraft-securingc7::services
include bkraft-securingc7::fail2ban
include bkraft-securingc7::general
include bkraft-securingc7::users
include bkraft-securingc7::openssh
```

## Limitations

CentOS 7 only