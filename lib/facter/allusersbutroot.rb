Facter.add('allusersbutroot') do
      setcode do
        Facter::Core::Execution.exec('/usr/bin/awk -F: \'!/root/{print $1}\' /etc/passwd')
      end
end
