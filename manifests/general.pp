class bkraft-securingc7::general	{
	
	augeas { "login.defs":
		context => "/files/etc/login.defs",
		changes => [
			"set PASS_MIN_LEN 9",
		],
	}	

	file { "/etc/profile.d/inactive-users-disconnect.sh":
		ensure =>	present,
		owner =>	root,
		group =>	root,
		mode =>		0644,
		content =>	"readonly TMOUT=900\nreadonly HISTFILE",
	}

	file { "/etc/profile.d/umask-enforcement.sh":
		ensure =>       present,
                owner =>        root,
                group =>        root,
                mode =>         0644,
                content =>      "umask 027",
	}

	file { "/etc/cron.allow":
		ensure =>	present,
		owner =>	root,
		group =>	root,
		mode =>		0600,
		content =>	'',
	}
	
	file { "/etc/at.allow":
		ensure =>	present,
		owner =>	root,
		group =>	root,
		mode =>		0600,
		content =>	'',
	}	

	file { "/etc/cron.deny":
		ensure =>	present,
		owner =>	root,
		group =>	root,
		mode =>		0600,
		content =>	"${allusersbutroot}",
	}	

	file { "/etc/at.deny":
		ensure =>	present,
		owner =>	root,
		group =>	root,
		mode =>		0600,
		content =>	"${allusersbutroot}",
	}	

	file { "/etc/issue":
		ensure =>	present,
		owner =>	root,
		group =>	root,
		mode =>		0644,
		content =>	"USE OF THIS COMPUTER SYSTEM, AUTHORIZED OR UNAUTHORIZED, CONSTITUTES CONSENT TO MONITORING OF THIS SYSTEM.\nUNAUTHORIZED USE MAY SUBJECT YOU TO CRIMINAL PROSECUTION.\nEVIDENCE OF UNAUTHORIZED USE COLLECTED DURING MONITORING MAY BE USED FOR ADMINISTRATIVE, CRIMINAL, OR OTHER ADVERSE ACTION.\nUSE OF THIS SYSTEM CONSTITUTES CONSENT TO MONITORING FOR THESE PURPOSES.\n",
	}

	file { ["/root",
		"/var/log/audit",
		"/etc/skel"]:
		owner =>        root,
                group =>        root,
                mode =>         0700,
	}

	file { ["/etc/rsyslog.conf",
                "/etc/sysctl.conf",]:
                owner =>        root,
                group =>        root,
                mode =>         0700,
        }

	file {"/sbin/iptables":
		owner =>        root,
                group =>        root,
                mode =>         0740,
	}

	file {"/etc/sysctl.d/securingc7.conf":
		ensure =>       present,
                owner =>        root,
                group =>        root,
                mode =>         0600,
		content =>	"puppet:///modules/bkraft-securingc7/sysctl-bkraft.conf",
	}

	file {"/etc/modprobe.d/blacklist.conf":
		ensure =>	present,
		owner =>	root,
		group =>	root,
		mode =>		0600,
		content =>	"blacklist pcspkr\nblacklist btusb\nblacklist bluetooth\nblacklist rfkill\nblacklist floppy\n",
	}
}
