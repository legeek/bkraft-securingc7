class bkraft-securingc7::openssh	{
	exec{"RunningExecBecauseAugeasSucksAtUncommenting":
		command =>	"/usr/bin/perl -i -pe 's/#PermitRootLogin yes/PermitRootLogin no/;s/#Banner.*/Banner \/etc\/issue/g;s/^#ServerKeyBits 1024/ServerKeyBits 2048/g;s/^#MaxAuthTries 6/MaxAuthTries 4/g;s/^#Protocol 2/Protocol 2/g;s/^#StrictModes yes/StrictModes yes/g;s/^#AllowTcpForwarding yes/AllowTcpForwarding no/g;s/X11Forwarding yes/X11Forwarding no/g' /etc/ssh/sshd_config",
	}
}
