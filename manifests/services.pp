class bkraft-securingc7::services	{
	stopanddisablecert{'iprinit':;
		'iprupdate':;
		'iprdump':;
		'avahi-daemon':;
		'NetworkManager':;
		'firewalld':;
	}
}

define stopanddisablecert()	{
	service	{$name:
		ensure	=>	stopped,
		enable	=>	false,
	}
}

