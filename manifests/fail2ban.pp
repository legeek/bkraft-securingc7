class bkraft-securingc7::fail2ban	{

	package { 'epel-release':
		ensure =>	present,
		allow_virtual	=>	false,
	}

	package { 'iptables-services':
		ensure =>	present,
		allow_virtual	=>	false,
	}
	
	service { 'iptables':
		ensure	=>	running,
		enable	=>	true,
		require	=>	Package['iptables-services'],
	}

	package { 'fail2ban':
		ensure	=>	present,
		allow_virtual	=>	false,
		require	=>	[Package['epel-release'], Package['iptables-services']]
	}

	service { 'fail2ban':
		ensure	=>	running,
		enable	=>	true,
		require	=>	[Package['fail2ban'], File['/etc/fail2ban/jail.conf']]
	}

	file{"/etc/fail2ban/jail.conf":
		ensure =>	present,
		checksum =>	md5,
		mode =>		0600,
		owner =>	"root",
		group =>	"root",
		replace =>	true,
		source =>	"puppet:///modules/bkraft-securingc7/fail2ban-jail.conf",
		require =>      Package['fail2ban'],
	}
}
